/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef JETTOOLHELPERS_JETTOOLHELPERSDICT_H
#define JETTOOLHELPERS_JETTOOLHELPERSDICT_H

#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

#include "JetToolHelpers/HistoInput1D.h"
#include "JetToolHelpers/HistoInput2D.h"
#endif

#include "../TrigMuonEFTrackIsolationTool.h"
#include "../TrigMuonEFTrackIsolationAlg.h"
#include "../MuonFilterAlg.h"
#include "../MuonChainFilterAlg.h"
#include "../MergeEFMuonsAlg.h"
#include "../MergeMuonInDetTracksAlg.h"

DECLARE_COMPONENT( TrigMuonEFTrackIsolationTool )
DECLARE_COMPONENT( TrigMuonEFTrackIsolationAlg )
DECLARE_COMPONENT( MuonFilterAlg )
DECLARE_COMPONENT( MuonChainFilterAlg )
DECLARE_COMPONENT( MergeEFMuonsAlg )
DECLARE_COMPONENT( MergeMuonInDetTracksAlg )


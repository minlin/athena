/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "DerivationFrameworkHI/HITrackQualityAugmentationTool.h"

using namespace DerivationFramework;

DECLARE_COMPONENT( HITrackQualityAugmentationTool )